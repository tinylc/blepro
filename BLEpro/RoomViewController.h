//
//  RoomViewController.h
//  BLEpro
//
//  Created by u on 14-3-4.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "slideView.h"
#import "upView.h"
#import "ModelAnalyse.h"
#import "EditRoomViewController.h"
#import "LightViewController.h"
#import "allRoomsCell.h"
#import "BLEproAppDelegate.h"
#import "tableUpView.h"

@interface RoomViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,slideViewDelegate>
@property (strong,nonatomic) UITableView *tableView;
@property(strong,nonatomic) NSMutableArray *rooms;
@property(strong,nonatomic) NSMutableArray *lights;
@property (strong,nonatomic) slideView *uiView;
@property (strong,nonatomic) upView *UPView;
@property (strong,nonatomic) tableUpView *tableUpView;
@property(nonatomic) int state;
@property (nonatomic) ModelAnalyse *analyse;

@end
