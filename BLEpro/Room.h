//
//  Room.h
//  BLEpro
//
//  Created by u on 14-3-18.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Room : NSManagedObject

@property (nonatomic, retain) NSString * rID;
@property (nonatomic, retain) NSString * rname;
@property (nonatomic, retain) NSString * rpicture;

@end
