//
//  RootViewController.m
//  BLEpro
//
//  Created by u on 14-5-28.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "RootViewController.h"
#import "allRoomsCell.h"

@interface RootViewController (){
    BLEproAppDelegate *blead;
}
//定义view坐标
@property struct rect
{
CGRect frect;
CGRect lrect;
};
@end
@implementation RootViewController
struct rect lv;
struct rect sv;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//设置代理，背景，导航栏
    blead = [[UIApplication sharedApplication]delegate];
    UIImageView *imv =  [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"slide1.png"]];
    imv.frame = self.view.frame;
    [self.view insertSubview:imv atIndex:0];
//设置标题
    _viewTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    _viewTitle.font = [UIFont systemFontOfSize:19];
    _viewTitle.textColor = [UIColor whiteColor];
    _viewTitle.backgroundColor = [UIColor clearColor];
    _viewTitle.textAlignment = NSTextAlignmentCenter;
    self.navigationItem.titleView = _viewTitle;
//设置左上角按钮
    UIButton*leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0,0,20,20)];
    [leftButton setImage:[UIImage imageNamed:@"slide.png"]forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(swipeRight)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
    self.navigationItem.leftBarButtonItem= leftItem;
//设置侧边栏和主view显示坐标
    lv.frect = CGRectMake(0, 64, 320, self.view.bounds.size.height-64);
    lv.lrect = CGRectMake(200, 84, 320, 320);
    sv.frect = CGRectMake(-200, 0, 200, 500);
    sv.lrect = CGRectMake(0, 0, 200, 500);
//设置侧边栏
    _slideView = [[slideView alloc]initWithFrame:sv.frect];
    _slideView.delegate = self;
    [self.view addSubview:_slideView];
//设置panstate;
    _panstate=nopan;
//设置主view
    _state=0;
    _currentView = light;
    _toplightview = [[UIView alloc]initWithFrame:lv.frect];
    _toplightview.clipsToBounds=YES;
    _toplightview.layer.masksToBounds = YES;
    [_toplightview setBackgroundColor:[UIColor colorWithRed:(float)48/255 green:(float)57/255 blue:(float)66/255 alpha:1.0]];
    [self.view addSubview: _toplightview];
    _lightView = [[listView alloc]initWithFrame:_toplightview.bounds];
    _lightView.delegate = self;
    [_toplightview addSubview: _lightView];
    _roomView = [[UITableView alloc] initWithFrame:_toplightview.bounds];
    _roomView.backgroundColor = [UIColor clearColor];
    _roomView.separatorStyle =UITableViewCellSeparatorStyleNone;
    [_roomView setDelegate:self];
    [_roomView setDataSource:self];
    [_toplightview addSubview:_roomView];
    _effectView = [[effectView alloc]initWithFrame:_toplightview.bounds];
    _effectView = [[[NSBundle mainBundle] loadNibNamed:@"effectView" owner:nil options:nil] firstObject];
    _effectView.delegate = self;
    [_toplightview addSubview:_effectView];
    
//设置当前显示页面的导航栏显示(标题和右上角按钮)和页面显示
    [self viewInit];
//设置roomView 中tableUpView
    _tableUpView = [[tableUpView alloc]init];
    _tableUpView.delegate =self;
//设置lightView中upView
    _upView = [[upView alloc]init];
    _upView.delegate=self;
    _upView.frame=CGRectMake(0, 0, 320, 70);
    _upView.alpha=0.0;
    [_upView initialButton:1];
    _upView.state=upViewOff;
    [_toplightview addSubview:_upView];
//data operation
    _ma = [[ModelAnalyse alloc]init];
    _lp = [[LightProvider alloc]init];
    _control = [_ma getAllControl];
    _rooms = [_ma getAllLightsOfEachRoom];
    
//注册刷新回调
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(lrefresh:)
                                                 name:@"lrefresh"
                                               object:nil];
}
-(void)viewWillAppear:(BOOL)animated{
    
    [self lrefresh:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)lrefresh:(NSNotification *)notification{
    if(![[notification object] isEqual:@"update"]){
//lightview refresh
    for(UIView *view1 in [_lightView subviews]){
        [view1 removeFromSuperview];
    }
    [_lightView refresh];
//roomview refresh
    _rooms =[_ma getAllLightsOfEachRoom];
    [_roomView reloadData];
//effectview refresh
    [_effectView refresh];
    }
}
-(void)slidetoview:(int)number{
    _currentView = number;
    [self viewInit];
}
//rightbarbuttonitem init
-(void)initRightBarButtonItem{
    _viewRbtn = [[UIButton alloc]initWithFrame:CGRectMake(0,0,20,20)];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:_viewRbtn];
    self.navigationItem.rightBarButtonItem= rightItem;
}
-(void)viewInit{
    switch (_currentView) {
        case light:
        {
            _viewTitle.text = NSLocalizedString(@"lightTitle",nil);
            [self initRightBarButtonItem];
            [_viewRbtn setImage:[UIImage imageNamed:@"ladd.png"]forState:UIControlStateNormal];
            [_viewRbtn addTarget:self action:@selector(pushLightAddViewController)forControlEvents:UIControlEventTouchUpInside];
            _lightView.alpha=1.0;
            _roomView.alpha=0.0;
            _effectView.alpha = 0.0;
            if(_upView.state==upViewOff){
                _upView.alpha=0.0;
            }
            else{
                _upView.alpha=1.0;
            }
            [self swipeLeft];
        }
            break;
        case room:
        {
            _viewTitle.text = NSLocalizedString(@"room",nil);
            [self initRightBarButtonItem];
            [_viewRbtn setImage:[UIImage imageNamed:@"roomedit.png"]forState:UIControlStateNormal];
            [_viewRbtn addTarget:self action:@selector(pushRoomEditViewContoller)forControlEvents:UIControlEventTouchUpInside];
            _lightView.alpha=0.0;
            _roomView.alpha=1.0;
            _effectView.alpha = 0.0;
            _upView.alpha=0.0;
            [self swipeLeft];
        }
            break;
        case effect:
        {
            _viewTitle.text = NSLocalizedString(@"rochEffect",nil);
            [self initRightBarButtonItem];
            _lightView.alpha=0.0;
            _roomView.alpha=0.0;
            _effectView.alpha = 1.0;
            _upView.alpha=0.0;
            [self swipeLeft];
        }
            break;
        default:
            break;
    }
}
#pragma mark upview回调
-(void)refreshView{
    [_ma setNoConnect];
    [self lrefresh:nil];
    NSMutableArray *lights = [_ma getAllLights];
    for(NSMutableDictionary *light in lights){
        if([[light valueForKey:@"lconfig"]isEqualToString:@"1"]){
            _bp=[_lp searchBLEfromLight:light];
            if(_bp!=NULL){
                if(_bp.activePeripheral.state==0){
                    [blead.ble connectPeripheral:_bp.activePeripheral];
                }else{
                    [light setObject:@"1" forKey:@"lconnect"];
                    [_ma changeLight:light];
                }
                [self lrefresh:nil];
            }
        }
    }
}
-(void)alopen{
    for(NSMutableArray *lights in [_rooms valueForKey:@"data"]){
        for(NSMutableDictionary *light in lights){
            if([[light valueForKey:@"lconnect"]isEqualToString:@"1"]){
                [[_lp searchBLEfromLight:light]InstClickOn:[light valueForKey:@"lcolor"]WithAlpha:[[light valueForKey:@"lbright"] intValue]];
                [light setObject:@"1" forKey:@"lstate"];
                [_ma changeLight:light];
            }
        }
    }
    [self lrefresh:nil];
}
-(void)alclose{
    for(NSMutableArray *lights in [_rooms valueForKey:@"data"]){
        for(NSMutableDictionary *light in lights){
            if([[light valueForKey:@"lconnect"]isEqualToString:@"1"]){
                [[_lp searchBLEfromLight:light]InstClickOff];
                [light setObject:@"0" forKey:@"lstate"];
                [_ma changeLight:light];
            }
        }
    }
    [self lrefresh:nil];
}
-(void)roomalopen{
    NSMutableArray *lights=[[_rooms objectAtIndex:_tableUpView.ipath.row]valueForKey:@"data"];
    for(NSMutableDictionary *light in lights){
        if([[light valueForKey:@"lconnect"]isEqualToString:@"1"]){
            [[_lp searchBLEfromLight:light]InstClickOn:[light valueForKey:@"lcolor"]WithAlpha:[[light valueForKey:@"lbright"] intValue]];
            [light setObject:@"1" forKey:@"lstate"];
            [_ma changeLight:light];
        }
    }
    [self tableView:_roomView didSelectRowAtIndexPath:_tableUpView.ipath];
    [self lrefresh:nil];
}
-(void)roomalclose{
    NSMutableArray *lights=[[_rooms objectAtIndex:_tableUpView.ipath.row]valueForKey:@"data"];
    for(NSMutableDictionary *light in lights){
        if([[light valueForKey:@"lconnect"]isEqualToString:@"1"]){
            [[_lp searchBLEfromLight:light]InstClickOff];
            [light setObject:@"0" forKey:@"lstate"];
            [_ma changeLight:light];
        }
    }
    [self tableView:_roomView didSelectRowAtIndexPath:_tableUpView.ipath];
    [self lrefresh:nil];
}

#pragma mark push view
-(void)pushRoomEditViewContoller{
    _ervc = [self.storyboard instantiateViewControllerWithIdentifier:@"editRoom"];
    [self.navigationController pushViewController:_ervc animated:YES];
}
-(void)pushLightAddViewController{
    if(_state==0){
    _lavc = [self.storyboard instantiateViewControllerWithIdentifier:@"lightAdd"];
    _lavc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self.navigationController pushViewController:_lavc animated:YES];
    }
}
- (void)pushLightBreathOrSceneViewController:(NSMutableDictionary *)light{
    if(_state==0){
    if([[light valueForKey:@"lbreathstate"]isEqualToString:@"1"]){
        _lbvc = [self.storyboard instantiateViewControllerWithIdentifier:@"lbreath"];
        _lbvc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        _lbvc.lightname.text = [light valueForKey:@"lname"];
        _lbvc.light = [[NSMutableDictionary alloc]init];
        _lbvc.light = light;
        [self.navigationController pushViewController:_lbvc animated:YES];
    }else{
        _lsvc = [self.storyboard instantiateViewControllerWithIdentifier:@"lightScene"];
        _lsvc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        _lsvc.light =[[NSMutableDictionary alloc]init];
        _lsvc.light = light;
        [self.navigationController pushViewController:_lsvc animated:YES];
    }
    }
}
-(void)pushEffectShakeViewController{
    if(_state==0){
    _sevc = [self.storyboard instantiateViewControllerWithIdentifier:@"shakeEffect"];
    _sevc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self.navigationController pushViewController:_sevc animated:NO];
    }
}
-(void)pushEffectRotateViewController{
    if(_state==0){
    _revc = [self.storyboard instantiateViewControllerWithIdentifier:@"rotateEffect"];
    _revc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self.navigationController pushViewController:_revc animated:NO];
    }
}
#pragma mark 向左向右向上向下滑动
- (IBAction)panGesture:(UIPanGestureRecognizer *) recognizer{
        float x=0;
        float y=self.view.bounds.size.height-64;
        CGPoint point = [recognizer locationInView:self.view];
        if(recognizer.state == UIGestureRecognizerStateBegan){
            _panbounds =CGPointMake(_toplightview.center.x-point.x, _toplightview.center.y-point.y) ;
            _panlightbounds =CGPointMake(_lightView.center.x-point.x, _lightView.center.y-point.y);
        }
        if(_panstate==nopan){
            if(_currentView==light){
                //纵向移动多
                 if(fabs(_panbounds.x-_toplightview.center.x+point.x)<=fabs(_panbounds.y-_toplightview.center.y+point.y)&&fabs(_panbounds.y-_toplightview.center.y+point.y)>10){
                    _panstate=toupordown;
                }
            }
                //横向移动多，且左右移动
            if(fabs(_panbounds.x-_toplightview.center.x+point.x)> fabs(_panbounds.y-_toplightview.center.y+point.y)&&fabs(_panbounds.x-_toplightview.center.x+point.x)>10){
                    _panstate=toright;
            }
        }
        if(_panstate==toupordown){
            y=point.y+_panlightbounds.y;
            NSLog(@"%f %d",y,_lightView.height);
            if(_lightView.height>_toplightview.bounds.size.height){
                if(y+_lightView.height-250>_toplightview.bounds.size.height-30&&y<350){
                    [_lightView setCenter:CGPointMake(_toplightview.bounds.size.width/2, y)];
                }
            }
            if(_lightView.height<=_toplightview.bounds.size.height){
                if(y>_toplightview.bounds.size.height/2-40&&y<_toplightview.bounds.size.height/2+100){
                    [_lightView setCenter:CGPointMake(_toplightview.bounds.size.width/2, y)];
                }
            }
            if(_lightView.frame.origin.y>50){
                _upView.alpha=1.0;
                _upView.state=upViewOn;
            }else{
                _upView.alpha=0.0;
                _upView.state=upViewOff;
            }
        }
        if(_panstate==toright){
            x=point.x+_panbounds.x;
            if(x>=130&&x<400){
                [_toplightview setCenter:CGPointMake(x, (0.9-y/400)*x+0.9*y-80)];
                [_toplightview setBounds:CGRectMake(0, 0, 320, (360-x)*(y-320)/200+320)];
                [_slideView setCenter:CGPointMake(x-260, 250)];
            }
        }
        if(recognizer.state ==UIGestureRecognizerStateEnded){
            if(_panstate==toright){
                if(_toplightview.frame.origin.x<120){
                    [self swipeLeft];
                }else if(_toplightview.frame.origin.x>100){
                    [self swipeRight];
                }
            }
            if(_panstate==toupordown){
                if(_lightView.frame.origin.y<=0&&_lightView.height<_toplightview.frame.size.height){
                    [self swipeUporDown:CGRectMake(0, 0, 320, _toplightview.frame.size.height)];
                }
                if(_lightView.frame.origin.y>50){
                    [self swipeUporDown:CGRectMake(0, 71, 320, _toplightview.frame.size.height)];
                }
                if(_lightView.frame.origin.y<=50&&_lightView.frame.origin.y>0){
                    [self swipeUporDown:CGRectMake(0, 0, 320, _toplightview.frame.size.height)];
                }
                if(_lightView.frame.origin.y+_lightView.height<_toplightview.frame.size.height&&_lightView.height>=_toplightview.frame.size.height){
                    [self swipeUporDown:CGRectMake(0,_toplightview.frame.size.height-_lightView.height-5, 320, _toplightview.frame.size.height)];
                }
            }
            _panstate=nopan;
        }
}
- (void)swipeLeft{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDelegate:self];
    self.navigationController.navigationBar.alpha = 1.0;
    _state=0;
    _slideView.frame = sv.frect;
    _toplightview.frame = lv.frect;
    [UIView commitAnimations];
}
- (void)swipeRight{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDelegate:self];
    self.navigationController.navigationBar.alpha = 0.0;
    _slideView.frame = sv.lrect;
    _toplightview.frame = lv.lrect;
    _state=1;
    [UIView commitAnimations];
}
-(void)swipeUporDown:(CGRect)point{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDelegate:self];
    _lightView.frame=point;
    [UIView commitAnimations];
}
#pragma mark tableView Call-back
- (void)tableView:(UITableView *)tableview didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_tableUpView.location == 1){
        _tableUpView.location =2;
        _tableUpView.ipath = indexPath;
        [_roomView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        allRoomsCell *cell = (allRoomsCell *)[_roomView cellForRowAtIndexPath:indexPath];
        _tableUpView =[[tableUpView alloc]initWithFrame:CGRectMake(0,0,320,100)];
        _tableUpView.delegate = self;
        _tableUpView.alpha=1.0;
        _tableUpView.location =2;
        _tableUpView.ipath = indexPath;
        //[cell insertSubview:_tableUpView atIndex:1];
        [cell.contentView addSubview:_tableUpView];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDelegate:self];
        _tableUpView.frame = CGRectMake(0, 100, 320, 100);
        _tableUpView.alpha = 1.0;
        [UIView commitAnimations];
    }
    else if(_tableUpView.location == 2){
        
        if(_tableUpView.ipath.row==indexPath.row){
            _tableUpView.location =1;
            _tableUpView.ipath =NULL;
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.5];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            [UIView setAnimationDelegate:self];
            _tableUpView.frame = CGRectMake(0, 0, 320, 100);
            _tableUpView.alpha = 0.0;
            [UIView commitAnimations];
            [_roomView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        else if(_tableUpView.ipath.row!=indexPath.row){
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.5];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            [UIView setAnimationDelegate:self];
            _tableUpView.frame = CGRectMake(0, 0, 320, 100);
            _tableUpView.alpha = 0.0;
            [UIView commitAnimations];
            NSArray *mute = [[NSArray alloc]initWithObjects:_tableUpView.ipath,indexPath,nil];
            _tableUpView.ipath =indexPath;
            [_roomView reloadRowsAtIndexPaths:mute withRowAnimation:UITableViewRowAnimationAutomatic];
            allRoomsCell *cell = (allRoomsCell *)[_roomView cellForRowAtIndexPath:indexPath];
            _tableUpView =[[tableUpView alloc]initWithFrame:CGRectMake(0,0,320,100)];
            _tableUpView.delegate = self;
            _tableUpView.alpha=0.0;
            _tableUpView.location =2;
            _tableUpView.ipath = indexPath;
            [cell.contentView addSubview:_tableUpView];
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:1];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            [UIView setAnimationDelegate:self];
            _tableUpView.frame = CGRectMake(0, 100, 320, 100);
            _tableUpView.alpha = 1.0;
            [UIView commitAnimations];
        }
        
    }
    [_roomView deselectRowAtIndexPath:indexPath animated:NO];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_rooms count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_tableUpView.location==2 &&(_tableUpView.ipath.row ==indexPath.row)){
        return 200;
    }
    return 100;
}
-(BOOL)canBecomeFirstResponder {
    return YES;
}
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Lrstatechange"
         object:nil
         userInfo:nil];
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    allRoomsCell *cell = (allRoomsCell *)[_roomView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"allRooms" owner:self options:nil];
        cell = [array objectAtIndex:0];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        //单个房间未连接的灯个数
        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"lconnect==%@",@"0"];
        ;
        [[cell disconnectlight]setText:[NSString stringWithFormat:@"%lu",(unsigned long)[[[_rooms objectAtIndex:indexPath.row]valueForKey:@"data"] filteredArrayUsingPredicate:predicate].count]];
        predicate=[NSPredicate predicateWithFormat:@"lconnect==%@",@"1"];
        //单个房间连接的灯个数
        NSArray *connectedlights=[[[_rooms objectAtIndex:indexPath.row]valueForKey:@"data"] filteredArrayUsingPredicate:predicate];
        predicate=[NSPredicate predicateWithFormat:@"lstate==%@",@"1"];
        //单个房间连接的ONOFF灯分别个数
        [[cell onlight]setText:[NSString stringWithFormat:@"%lu",(unsigned long)[connectedlights filteredArrayUsingPredicate:predicate].count]];
        predicate=[NSPredicate predicateWithFormat:@"lstate==%@",@"0"];
        [[cell offlight]setText:[NSString stringWithFormat:@"%lu",(unsigned long)[connectedlights filteredArrayUsingPredicate:predicate].count]];
        [[cell rname]setText:[[_rooms objectAtIndex:indexPath.row] valueForKey:@"rname"]];
    }
    [cell setBackgroundColor:[UIColor clearColor]];
    return cell;
}

@end
