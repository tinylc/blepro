//
//  LightTextViewController.m
//  BLEpro
//
//  Created by u on 14-2-26.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "LightTextViewController.h"

@interface LightTextViewController ()
- (IBAction)endEdit:(id)sender;
- (IBAction)saveLightName:(id)sender;



@end

@implementation LightTextViewController
@synthesize lightText;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _ma = [[ModelAnalyse alloc]init];
    _lp = [[LightProvider alloc]init];
    if(![[_light valueForKey:@"lname" ] isEqualToString:NULL])
        lightText.text = [_light valueForKey:@"lname" ];
    UILabel *t = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    t.font = [UIFont systemFontOfSize:19];
    t.textColor = [UIColor whiteColor];
    t.backgroundColor = [UIColor clearColor];
    t.textAlignment = NSTextAlignmentCenter;
    t.text = NSLocalizedString(@"inputLight", nil);
    
    self.navigationItem.titleView = t;
	//注册刷新回调
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(lrefresh:)
                                                 name:@"lrefresh"
                                               object:nil];
}
-(void)lrefresh:(NSNotification *)notification{
    if(self.view.window!=nil){
        if([[notification object] isEqual:@"powerOff"]||[[notification object] isEqual:@"disconnect"]){
            NSDictionary *theData = [notification userInfo];
            blePeripheral *bp=[theData valueForKey:@"ble"];
            if([[_light valueForKey:@"lnameID"]isEqualToString:bp.uuidString]){
                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"loseConnection",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok",nil)  otherButtonTitles:nil, nil];
                [alert1 show];
                [alert1 setTag:1];
            }
        }
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==1){
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}
-(BOOL)canBecomeFirstResponder {
    return YES;
}
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Lrstatechange"
         object:nil
         userInfo:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)endEdit:(id)sender {
    if([lightText.text isEqual:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"notNull", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil, nil];
        [alert show];
    }else{
        [sender resignFirstResponder];
    }
}

- (IBAction)saveLightName:(id)sender {
    if([lightText.text isEqual:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"notNull", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok", nil) otherButtonTitles:nil, nil];
        [alert show];
    }else{
        [_light setObject:lightText.text forKey:@"lname"];
        [_ma changeLight:_light];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
