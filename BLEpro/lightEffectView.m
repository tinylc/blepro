//
//  lightEffectView.m
//  BLEpro
//
//  Created by u on 14-5-9.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "lightEffectView.h"

@implementation lightEffectView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        _light = [[NSMutableDictionary alloc]init];
        _ma = [[ModelAnalyse alloc]init];
        _lp =[[LightProvider alloc]init];
    }
    return self;
}
-(void)setState:(int)state{
    _state=state;
    
    _btn = [[UIButton alloc]initWithFrame:CGRectMake(36, 23, 35, 55)];
    _btn.backgroundColor =[UIColor clearColor];
    [_btn setBackgroundImage:[UIImage imageNamed:@"lon.png"] forState:UIControlStateNormal];
    _sel = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"select.png"]];
    _sel.frame=CGRectMake(48, 15, 40, 30);
    if(_state==0){
        if([[_light valueForKey:@"lrstate"]isEqualToString:@"1"]){
            _sel.alpha=1.0;
        }else{
            _sel.alpha=0.0;
        }
    }else{
        if([[_light valueForKey:@"lrostate"]isEqualToString:@"1"]){
            _sel.alpha=1.0;
        }else{
            _sel.alpha=0.0;
        }
    }
    [_btn addTarget:self action:@selector(ltouch) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_btn];
    [self addSubview:_sel];
    
    _lname = [[UILabel alloc]initWithFrame:CGRectMake(0, 95, 320/3, 16)];
    _lname.text = [_light valueForKey:@"lname"];
    _lname.textColor = [UIColor whiteColor];
    _lname.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_lname];
}
-(void)ltouch{
    if(_state==0){
        if([[_light valueForKey:@"lrstate"]isEqualToString:@"0"]){
            [_light setObject:@"1" forKey:@"lrstate"];
            [_ma changeLight:_light];
            _sel.alpha=1.0;
        }else if([[_light valueForKey:@"lrstate"]isEqualToString:@"1"]){
            [_light setObject:@"0" forKey:@"lrstate"];
            [_ma changeLight:_light];
            _sel.alpha=0.0;
        }
    }else{
        if([[_light valueForKey:@"lrostate"]isEqualToString:@"0"]){
            [_light setObject:@"1" forKey:@"lrostate"];
            [_ma changeLight:_light];
            _sel.alpha=1.0;
        }else if([[_light valueForKey:@"lrostate"]isEqualToString:@"1"]){
            [_light setObject:@"0" forKey:@"lrostate"];
            [_ma changeLight:_light];
            _sel.alpha=0.0;
        }
    }
}
@end
