//
//  RotateEffectViewController.m
//  BLEpro
//
//  Created by u on 14-6-25.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "RotateEffectViewController.h"

@interface RotateEffectViewController ()

@end

@implementation RotateEffectViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _state=RotateNo;
    _ma = [[ModelAnalyse alloc]init];
    _motionManager =[[CMMotionManager alloc]init];
    _queue =[[NSOperationQueue alloc]init];
    _lp = [[LightProvider alloc]init];
    _control = [_ma getAllControl];
    _motionManager.accelerometerUpdateInterval=1/10.0;
    if([[_control valueForKey:@"lrocontrol"]isEqualToString:@"1"]){
        if(_motionManager.accelerometerAvailable){
            [_motionManager startAccelerometerUpdatesToQueue:_queue withHandler:^(CMAccelerometerData *accelerometerData, NSError *error)
             {
                 double x = accelerometerData.acceleration.x;
                 double y = accelerometerData.acceleration.y;
                 double z = accelerometerData.acceleration.z;
                 if(_state==RotateNo){
                     if(x>0.6&&fabs(y)<0.4){
                         NSLog(@"+%f %f %f",x,y,z);
                         [self lightBrightIncrease];
                         _state=RotateYes;
                     }
                     if(x<-0.6&&fabs(y)<0.4){
                         NSLog(@"-%f %f %f",x,y,z);
                         [self lightBrightDecrease];
                         _state=RotateYes;
                     }
                 }else if(_state==RotateYes){
                     if(fabs(x)<0.3&&y<-0.6){
                         NSLog(@"=%f %f %f",x,y,z);
                         _state=RotateNo;
                     }
                 }
             }];
        }
    }
    _lev = [[lightsEffectView alloc]initWithFrame:CGRectMake(0, 64, 320, self.view.bounds.size.height-64)];
    [_lev refresh:1];
    [self.view addSubview:_lev];
    //注册刷新回调
    [[NSNotificationCenter defaultCenter] addObserver:self
                                         selector:@selector(lrefresh)
                                             name:@"lrefresh"
                                           object:nil];
// Do any additional setup after loading the view.
}
-(void)viewWillDisappear:(BOOL)animated{
    [_motionManager stopAccelerometerUpdates];
}
-(void)lrefresh{
    for(UIView *view1 in [_lev subviews]){
        [view1 removeFromSuperview];
    }
    [_lev refresh:1];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)lightBrightIncrease{
    _connectlights = [_ma getAllConnectedLights];
    for(NSMutableDictionary *light in _connectlights){
        if([[light valueForKey:@"lrostate"]isEqualToString:@"1"]){
            blePeripheral *bp = [_lp searchBLEfromLight:light];
            if([[light valueForKey:@"lbright"]intValue]>=205){
                [light setObject:@"255" forKey:@"lbright"];
            }else{
                [light setObject:[NSString stringWithFormat:@"%d",[[light valueForKey:@"lbright"]intValue]+50]forKey:@"lbright"] ;
            }
            [_ma changeLight:light];
            [bp InstClickOn:[light valueForKey:@"lcolor"] WithAlpha:[[light valueForKey:@"lbright"]intValue]];
        }
    }
}
-(void)lightBrightDecrease{
    _connectlights = [_ma getAllConnectedLights];
    for(NSMutableDictionary *light in _connectlights){
        if([[light valueForKey:@"lrostate"]isEqualToString:@"1"]){
            blePeripheral *bp = [_lp searchBLEfromLight:light];
            if([[light valueForKey:@"lbright"]intValue]<=50){
                [light setObject:@"0" forKey:@"lbright"];
            }else{
                [light setObject:[NSString stringWithFormat:@"%d",[[light valueForKey:@"lbright"]intValue]-50]forKey:@"lbright"] ;
            }
            [_ma changeLight:light];
            [bp InstClickOn:[light valueForKey:@"lcolor"] WithAlpha:[[light valueForKey:@"lbright"]intValue]];
        }
    }
}

@end
