//
//  MotionNotifacation.m
//  BLEpro
//
//  Created by u on 14-5-11.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "MotionNotifacation.h"
#import "UIColor-Expanded.h"

@implementation MotionNotifacation{
    BLEproAppDelegate   *blead;
}
-(id)init{
    self = [super init];
    if (self) {
        blead = [[UIApplication sharedApplication]delegate];
        
        _lights = [[NSMutableArray alloc]init];
        _control = [[NSMutableDictionary alloc]init];
        _lp = [[LightProvider alloc]init];
        _analyse = [[ModelAnalyse alloc]init];
        _da = [[DateAnalyse alloc]init];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(lrStateChange)
                                                     name:@"Lrstatechange"
                                                   object:nil];
        nCBCentralStateChange
        nCBPeripheralStateChange
    }
    return self;
}
-(void)lrStateChange{
    _lights = [_analyse getAllConnectedLights];
    _control = [_analyse getAllControl];
    if([[_control valueForKey:@"lrcontrol"]isEqualToString:@"1"]){
        if([[_control valueForKey:@"lrstate"]isEqualToString:@"1"]){
            [_control setObject:@"0" forKey:@"lrstate"];
            [_analyse changeControl:_control];
            for(int i=0;i<_lights.count;i++){
                if([[[_lights objectAtIndex:i]valueForKey:@"lrstate"]isEqualToString:@"1"]){
                    [[_lights objectAtIndex:i]setObject:@"0" forKey:@"lstate"];
                    [_analyse changeLight:[_lights objectAtIndex:i]];
                    if([[[_lights objectAtIndex:i]valueForKey:@"lgradstate"]isEqualToString:@"0"])
                    {
                        [[_lp searchBLEfromLight:[_lights objectAtIndex:i]] InstClickOff];
                    }else{
                        [[_lp searchBLEfromLight:[_lights objectAtIndex:i]] GradualClickOff:[[_lights objectAtIndex:i]valueForKey:@"lcolor"] WithAlpha:[[[_lights objectAtIndex:i]valueForKey:@"lbright"] intValue]];
                    }
                }
            }
        }
        else if([[_control valueForKey:@"lrstate"]isEqualToString:@"0"]){
            [_control setObject:@"1" forKey:@"lrstate"];
            [_analyse changeControl:_control];
            for(int i=0;i<_lights.count;i++){
                if([[[_lights objectAtIndex:i]valueForKey:@"lrstate"]isEqualToString:@"1"]){
                    [[_lights objectAtIndex:i]setObject:@"1" forKey:@"lstate"];
                    [_analyse changeLight:[_lights objectAtIndex:i]];
                    if([[[_lights objectAtIndex:i]valueForKey:@"lgradstate"]isEqualToString:@"0"])
                    {[[_lp searchBLEfromLight:[_lights objectAtIndex:i]] InstClickOn:[[_lights objectAtIndex:i]valueForKey:@"lcolor"]WithAlpha:[[[_lights objectAtIndex:i]valueForKey:@"lbright"] intValue]];
                    }
                    else{
                        [[_lp searchBLEfromLight:[_lights objectAtIndex:i]] GradualClickOn:[[_lights objectAtIndex:i]valueForKey:@"lcolor"]WithAlpha:[[[_lights objectAtIndex:i]valueForKey:@"lbright"] intValue]];
                    }
                }
            }
        }
    }
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"lrefresh"
     object:@"lshake"
     userInfo:nil];
}
#pragma mark - 通知回调函数
-(void)CBCentralStateChange{
    blePeripheral *bp=blead.ble.bp;
    NSLog(@"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%lu",(unsigned long)bp.currentPeripheralState);
    NSLog(@"%lu",(unsigned long)blead.ble.currentCentralManagerState);
    if(blead.ble.currentCentralManagerState==bleCentralDelegateStateCentralManagerPoweredOff){
        [_analyse setNoConnect];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"lrefresh"
         object:@"powerOff"
         userInfo:[NSDictionary dictionaryWithObject:bp forKey:@"ble"]];
    }
    //周边断开
    if(blead.ble.currentCentralManagerState==bleCentralDelegateStateDisconnectPeripheral){
        //连接超时返回该状态，已连接蓝牙设备失去连接
        [_analyse disconnectLight:bp.uuidString];
        [blead.ble resetScanning];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"lrefresh"
         object:@"disconnect"
         userInfo:[NSDictionary dictionaryWithObject:bp forKey:@"ble"]];
    }
    //发现周边
    if(blead.ble.currentCentralManagerState==bleCentralDelegateStateDiscoverPeripheral){
        NSMutableArray *uuid = [[NSMutableArray alloc]init];
        NSMutableDictionary *bpuuid= [[NSMutableDictionary alloc]init];
        [bpuuid setObject:bp.uuidString forKey:@"lnameID"];
        [uuid addObject:bpuuid];
        [_analyse addLights:uuid];
        _lights=_analyse.getAllLights;
        NSMutableDictionary *light;
        //连接数据库中显示为连接并且配置过的灯
        
        for(light in _lights){
            if([[light valueForKey:@"lconnect"]isEqualToString:@"1"]&&[[light valueForKey:@"lconfig"]isEqualToString:@"1"]){
                blePeripheral *bp;
                bp=[_lp searchBLEfromLight:light];
                if (bp.activePeripheral.state==0) {
                    [blead.ble connectPeripheral:bp.activePeripheral];
                }
            }
        }
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"lrefresh"
         object:@"discover"
         userInfo:nil];
    }
    if(blead.ble.currentCentralManagerState==bleCentralDelegateStateConnectPeripheral){
        _lights=_analyse.getAllLights;
        NSMutableDictionary *light=[_lp searchLightfromBLE:bp WithArray:_lights];
        [light setObject:@"1" forKey:@"lconnect"];
        [_analyse changeLight:light];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"lrefresh"
         object:nil
         userInfo:nil];
    }
}
-(void)CBPeripheralStateChange:(NSNotification*)notification{
    
    NSDictionary *data=[notification userInfo];
    NSPredicate *predicate;
    blePeripheral *bp=[data valueForKey:@"bleperipheral"];
    NSLog(@"%lu",(unsigned long)bp.currentPeripheralState);
    NSMutableDictionary *li;
    _lights = [_analyse getAllLights];
    if(bp.currentPeripheralState ==blePeripheralDelegateStateUpdate3Characteristics){
        predicate=[NSPredicate predicateWithFormat:@"lnameID==%@",bp.uuidString];
        li = [[_lights filteredArrayUsingPredicate:predicate]firstObject];
        Byte *test = (Byte *)[bp.DataCharateristicUUID2.value bytes];
        NSLog(@"%d %d %d %d",test[0],test[1],test[2],test[3]);
        if(test!=NULL){
            UIColor *color = [UIColor colorWithRed:(float)((int)test[1]/255) green:(float)(int)test[2]/255 blue:(float)(int)test[3]/255 alpha:(float)(int)test[0]/255];
            NSLog(@"%@",color.hexStringFromColor);
            if(![color.hexStringFromColor isEqualToString:@"00000000"]){
                [li setObject:[color.hexStringFromColor substringToIndex:6] forKey:@"lcolor"];
                NSLog(@"%@",[color.hexStringFromColor substringToIndex:6]);
                [li setObject:[NSString stringWithFormat:@"%d",test[0]] forKey:@"lbright"];
            }
            if((int)test[0]!=0){
                [li setObject:[NSString stringWithFormat:@"%d",1] forKey:@"lstate"];
            }else{
                [li setObject:[NSString stringWithFormat:@"%d",0] forKey:@"lstate"];
            }
        }
        [li setObject:@"1" forKey:@"lconnect"];
        [_analyse changeLight:li];
        if(bp.TimeCharateristicUUID5!=nil){
            [bp readTimeProperties];
        }
    }
    if(bp.currentPeripheralState ==blePeripheralDelegateStateUpdateCharacteristics){
        predicate=[NSPredicate predicateWithFormat:@"lnameID==%@",bp.uuidString];
        li = [[_lights filteredArrayUsingPredicate:predicate]firstObject];
        Byte *test = (Byte *)[bp.TimeCharateristicUUID2.value bytes];
        if(test!=NULL){
            if(test[2]==255&&test[3]==255){
                //
            }else if(test[4]==255){
                if(test[0]>=16){
                    _timer = [_analyse getTimeOfLight:li];
                    NSDate *date = [_da getDateWithHour:test[3] WithMinute:test[2]];
                    NSMutableDictionary *time;
                    time = [_timer objectAtIndex:(test[0]-16)];
                    [time setObject:date forKey:@"tdate"];
                    if(test[9]>0){
                        [time setObject:@"1" forKey:@"tstate"];
                    }else{
                        [time setObject:@"0" forKey:@"tstate"];
                    }
                    [_analyse changeTime:time];
                }else if(test[0]==8){
                    
                    NSDate *date = [_da getDateWithHour:test[3] WithMinute:test[2]];
                    [li setObject:date forKey:@"lwaketime"];
                }
                [li setObject:@"1" forKey:@"lconnect"];
                [_analyse changeLight:li];
            }else if(test[0]>=16){
                _timer = [_analyse getTimeOfLight:li];
                NSMutableDictionary *time;
                time = [_timer objectAtIndex:(test[0]-16)];
                [time setObject:@"2" forKey:@"tstate"];
                [_analyse changeTime:time];
                [li setObject:@"1" forKey:@"lconnect"];
                [_analyse changeLight:li];
            }
        }
    }
    if(bp.currentPeripheralState ==blePeripheralDelegateStateUpdate2Characteristics){
        predicate=[NSPredicate predicateWithFormat:@"lnameID==%@",bp.uuidString];
        li = [[_lights filteredArrayUsingPredicate:predicate]firstObject];
        Byte *test = (Byte *)[bp.TimeCharateristicUUID3.value bytes];
        if(test!=NULL){
            //定时设置没有全用。所以判断呼吸灯开启没有时判断定时前面几位从而判断当前晨醒的开启与否
            if(test[3]<16){
                if(test[2]==0){
                    [li setObject:@"0" forKey:@"lwakestate"];
                }else{
                    [li setObject:@"1" forKey:@"lwakestate"];
                }
                [li setObject:@"0" forKey:@"lbreathstate"];
                NSLog(@"lwakestate %@",[li valueForKey:@"lwakestate"]);
            }else{
                [li setObject:@"1" forKey:@"lbreathstate"];
            }
        }
        [li setObject:@"1" forKey:@"lconnect"];
        [_analyse changeLight:li];
    }
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"lrefresh"
     object:@"update"
     userInfo:nil];
}

@end
