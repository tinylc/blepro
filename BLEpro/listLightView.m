//
//  listLightView.m
//  BLEpro
//
//  Created by u on 14-5-20.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "listLightView.h"

@implementation listLightView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        _light = [[NSMutableDictionary alloc]init];
        _ma = [[ModelAnalyse alloc]init];
        _lp =[[LightProvider alloc]init];
    }
    return self;
}
-(void)setState{
    _btn = [[UIButton alloc]initWithFrame:CGRectMake(36, 23, 35, 55)];
    _btn.backgroundColor =[UIColor clearColor];
    if([[_light valueForKey:@"lconnect"]isEqualToString:@"1"]){
        if([[_light valueForKey:@"lstate"]isEqualToString:@"0"]){
            [_btn setBackgroundImage:[UIImage imageNamed:@"loff.png"] forState:UIControlStateNormal];
        }else if([[_light valueForKey:@"lstate"]isEqualToString:@"1"]){
            [_btn setBackgroundImage:[UIImage imageNamed:@"lon.png"] forState:UIControlStateNormal];
        }
        [_btn addTarget:self action:@selector(ltouch) forControlEvents:UIControlEventTouchUpInside];
    }else {
        [_btn setBackgroundImage:[UIImage imageNamed:@"ldisabled.png"] forState:UIControlStateNormal];
    }
    [self addSubview:_btn];
    
    _lname = [[UILabel alloc]initWithFrame:CGRectMake(0, 95, 320/3, 16)];
    _lname.text = [_light valueForKey:@"lname"];
    _lname.font = [UIFont systemFontOfSize:16];
    if([[_light valueForKey:@"lconnect"]isEqualToString:@"1"]){
        _lname.textColor = [UIColor whiteColor];
    }else{
        _lname.textColor = [UIColor grayColor];
    }
    _lname.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_lname];
}
-(void)ltouch{
    if ([_delegate respondsToSelector:@selector(pushLightBreathOrSceneViewController:)]) {
        [_delegate pushLightBreathOrSceneViewController:_light];
    }
}
@end
