//
//  ShakeEffectViewController.h
//  BLEpro
//
//  Created by u on 14-5-22.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "lightsEffectView.h"
@interface ShakeEffectViewController : UIViewController
@property(nonatomic) ModelAnalyse *analyse;
@property (nonatomic) NSMutableArray *lights;
@property (nonatomic)lightsEffectView *lev;
@property (nonatomic) LightProvider *lp;
@end
