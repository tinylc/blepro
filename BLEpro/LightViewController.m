//
//  LightViewController.m
//  BLEpro
//
//  Created by u on 14-3-3.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "LightViewController.h"
#import "UIColor-Expanded.h"

@interface LightViewController (){
    BLEproAppDelegate   *blead;
}
@property struct rect
{
CGRect frect;
CGRect lrect;
};
@end

@implementation LightViewController
@synthesize uiView;
struct rect lv;
struct rect sv;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    blead = [[UIApplication sharedApplication]delegate];
   
    //navgation bar
    //[self.view setBackgroundColor:[UIColor colorWithRed:(float)48/255 green:(float)56/255 blue:(float)64/255 alpha:1.0]];
    UIImageView *imv =  [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"slide1.png"]];
    imv.frame = self.view.frame;
    [self.view insertSubview:imv atIndex:0];
    
    UILabel *t = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    t.font = [UIFont systemFontOfSize:19];
    t.textColor = [UIColor whiteColor];
    t.backgroundColor = [UIColor clearColor];
    t.textAlignment = NSTextAlignmentCenter;
    t.text = NSLocalizedString(@"lightTitle",nil);
    self.navigationItem.titleView = t;
    
    UIButton*rightButton = [[UIButton alloc]initWithFrame:CGRectMake(0,0,20,20)];
    [rightButton setImage:[UIImage imageNamed:@"ladd.png"]forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(addLight:)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem= rightItem;
    
    UIButton*leftButton = [[UIButton alloc]initWithFrame:CGRectMake(0,0,20,20)];
    [leftButton setImage:[UIImage imageNamed:@"slide.png"]forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(swipeRight:)forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
    self.navigationItem.leftBarButtonItem= leftItem;
    
//ListView init
    
    lv.frect = CGRectMake(0, 64, 320, self.view.bounds.size.height-64);
    lv.lrect = CGRectMake(200, 85, 320, 340	);
    sv.frect = CGRectMake(-250, 0, 250, 500);
    sv.lrect = CGRectMake(0, 0, 250, 500);
    _lv=[[listView alloc]init];
    _lv.delegate =self;
    if(_state==1){
        uiView = [[slideView alloc]initWithFrame:sv.lrect];
        _lv.frame=lv.lrect;
        uiView.location = 2;
        [self swipeLeft:self];
    }
    else{
        uiView = [[slideView alloc]initWithFrame:sv.frect];
        _lv.frame=lv.frect;
    }
    uiView.delegate = self;
    [self.view addSubview:_lv];
    [self.view addSubview:uiView];
    //注册刷新回调
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(lrefresh)
                                                 name:@"lrefresh"
                                               object:nil];
}
-(void)viewWillAppear:(BOOL)animated{
    [self lrefresh];
}
-(void)lrefresh{
    for(UIView *view1 in [_lv subviews]){
        [view1 removeFromSuperview];
    }
    [_lv refresh];
}
-(BOOL)canBecomeFirstResponder {
    return YES;
}
- (void)pushViewController:(NSMutableDictionary *)light{
    if([[light valueForKey:@"lbreathstate"]isEqualToString:@"1"]){
        _lbvc = [self.storyboard instantiateViewControllerWithIdentifier:@"lbreath"];
        _lbvc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        _lbvc.lightname.text = [light valueForKey:@"lname"];
        _lbvc.light = [[NSMutableDictionary alloc]init];
        _lbvc.light = light;
        [self.navigationController pushViewController:_lbvc animated:YES];
    }else{
        _lsvc = [self.storyboard instantiateViewControllerWithIdentifier:@"lightScene"];
        _lsvc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        _lsvc.light =[[NSMutableDictionary alloc]init];
        _lsvc.light = light;
        [self.navigationController pushViewController:_lsvc animated:YES];
    }
}
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Lrstatechange"
         object:nil
         userInfo:nil];
    }
}
-(void)slidetoview:(int)number{
    switch (number) {
        case 1:
            [self swipeLeft:self];
            break;
        case 2:
        {
            RoomViewController *rvc = [self.storyboard instantiateViewControllerWithIdentifier:@"room"];
            rvc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            rvc.state = 1;
            [self.navigationController pushViewController:rvc animated:NO];
        }
            break;
        case 3:
        {
            EffectViewController *evc = [self.storyboard instantiateViewControllerWithIdentifier:@"effect"];
            evc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            evc.state = 1;
            [self.navigationController pushViewController:evc animated:NO];
        }
            break;
        default:
            break;
    }
}
- (IBAction)swipeDown:(id)sender {
    if (uiView.location == 1){
        [UIView beginAnimations:nil context:nil];
        //持续时间
        [UIView setAnimationDuration:1.0];
        //在出动画的时候减缓速度
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        //添加动画开始及结束的代理
        [UIView setAnimationDelegate:self];
        //[UIView setAnimationDidStopSelector:@selector(viewButton)];
        //动画效果
        [UIView commitAnimations];
    }
}
- (IBAction)swipeUP:(id)sender {
    if (1){
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1.0];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDelegate:self];
        [UIView commitAnimations];
    }
}
- (IBAction)swipeLeft:(id)sender {
    if (uiView.location == 2){
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1.0];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDelegate:self];
        self.navigationController.navigationBar.alpha = 1.0;
        uiView.frame = sv.frect;
        _lv.frame = lv.frect;
        [UIView commitAnimations];
        uiView.location = 1;
    }
}
- (IBAction)swipeRight:(UISwipeGestureRecognizer *)sender {
    if(uiView.location == 1 ){
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1.0];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDelegate:self];
        self.navigationController.navigationBar.alpha = 0.0;
        uiView.frame = sv.lrect;
         _lv.frame = lv.lrect;
        [UIView commitAnimations];
        uiView.location = 2;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    //[tableView release];
}

-(void)addLight:(id)sender{
    //添加灯页面跳转
    _lavc = [self.storyboard instantiateViewControllerWithIdentifier:@"lightAdd"];
    _lavc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self.navigationController pushViewController:_lavc animated:YES];
}
//连接不上时
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0){
    }
    else if(buttonIndex==1){
        //
    }
}



@end
