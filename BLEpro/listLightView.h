//
//  listLightView.h
//  BLEpro
//
//  Created by u on 14-5-20.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelAnalyse.h"
#import "LightProvider.h"
@protocol lightSceneViewDelegate
@required
- (void)pushLightBreathOrSceneViewController:(NSMutableDictionary *)light;
@end

@interface listLightView : UIView
@property (nonatomic) NSMutableDictionary *light;
@property (nonatomic) UIButton *btn;
@property (nonatomic) UILabel *lname;
@property (nonatomic) ModelAnalyse *ma;
@property (nonatomic) LightProvider *lp;
@property (nonatomic) id delegate;
-(void)setState;
@end
