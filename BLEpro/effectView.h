//
//  effectView.h
//  BLEpro
//
//  Created by u on 14-5-30.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelAnalyse.h"
@protocol lightEffectViewDelegate
@required
- (void)pushEffectShakeViewController;
- (void)pushEffectRotateViewController;
@end
@interface effectView : UIView
@property (nonatomic) NSMutableArray *connectedlights;
@property (nonatomic) ModelAnalyse *ma;
@property (nonatomic) NSMutableDictionary *control;
@property (weak, nonatomic) IBOutlet UISwitch *lightesswitch;
@property (weak, nonatomic) IBOutlet UISwitch *lighterswitch;
@property (weak, nonatomic) IBOutlet UILabel *lightesnum;
@property (weak, nonatomic) IBOutlet UILabel *lighternum;
@property (nonatomic) id delegate;
@property (weak, nonatomic) IBOutlet UILabel *slabel;
@property (weak, nonatomic) IBOutlet UIButton *salabel;
@property (weak, nonatomic) IBOutlet UILabel *rlabel;
@property (weak, nonatomic) IBOutlet UIButton *ralabel;

-(void)refresh;
@end
