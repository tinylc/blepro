//
//  listView.m
//  BLEpro
//
//  Created by u on 14-5-19.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "listView.h"

@implementation listView{
    BLEproAppDelegate   *blead;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        blead = [[UIApplication sharedApplication]delegate];
        _ma = [[ModelAnalyse alloc]init];
        _rooms = [[NSMutableArray alloc]init];
        [self setBackgroundColor:[UIColor clearColor]];
            }
    return self;
}
-(void)refresh{
    
    _rooms = [_ma getAllLightsOfEachRoom];
    _height=0;
    for(int i=0;i<_rooms.count;i++){
        NSMutableArray *lights;
        lights = [[_rooms objectAtIndex:i] valueForKey:@"data"];
        _roomName =[[UILabel alloc]initWithFrame:CGRectMake(0,_height, 320, 28)];
        _roomName.textColor = [UIColor blackColor];
        _roomName.font =[UIFont systemFontOfSize:13];
        NSString *a =@"   ";
        _roomName.text = [a stringByAppendingString:[[_rooms objectAtIndex:i]valueForKey:@"rname"]];
        [_roomName setBackgroundColor:[UIColor colorWithRed:(float)89/255 green:(float)97/255 blue:(float)115/255 alpha:1.0]];
        [self addSubview:_roomName];
        for(int j=0;j<lights.count;j++){
            _lev = [[listLightView alloc]initWithFrame:CGRectMake((int)(j%3*(int)(320/3)), (int)(j/3*120)+24+_height, 120, 120)];
            _lev.delegate = _delegate;
            _lev.light = [lights objectAtIndex:j];
            [_lev setState];
            [self addSubview:_lev];
            [[[[_rooms objectAtIndex:i] valueForKey:@"data"]objectAtIndex:j]setObject:_lev forKey:@"view"];
        }
        _height=_height+(int)((lights.count-1)/3+1)*120+24;
    }
    UIImage *bg =[UIImage imageNamed:@"tableviewline.png"];
    UIImageView *tableBg = [[UIImageView alloc] initWithImage:bg];
    tableBg.frame=CGRectMake(0,_height+10,320,3);
    [self addSubview:tableBg];

}


@end
