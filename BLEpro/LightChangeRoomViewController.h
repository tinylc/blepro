//
//  LightChangeRoomViewController.h
//  BLEpro
//
//  Created by u on 14-5-21.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelAnalyse.h"
#import "blePeripheral.h"
@interface LightChangeRoomViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong,nonatomic) UITableView *tableView;
@property (nonatomic) NSMutableArray *rooms;
@property (nonatomic) ModelAnalyse *ma;
@property (nonatomic) NSMutableDictionary *light;
@end
