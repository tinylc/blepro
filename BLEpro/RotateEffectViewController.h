//
//  RotateEffectViewController.h
//  BLEpro
//
//  Created by u on 14-6-25.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>
#import "lightsEffectView.h"
#import "ModelAnalyse.h"
enum{
    RotateNo=0,
    RotateYes,
};
@interface RotateEffectViewController : UIViewController
@property (nonatomic) CMMotionManager *motionManager;
@property (nonatomic) NSOperationQueue *queue;
@property (nonatomic)lightsEffectView *lev;
@property (nonatomic) int state;
@property (nonatomic) NSMutableArray *connectlights;
@property (nonatomic) ModelAnalyse *ma;
@property (nonatomic) LightProvider *lp;
@property (nonatomic) NSMutableDictionary *control;
@end
