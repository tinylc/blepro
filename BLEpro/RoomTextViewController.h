//
//  RoomTextViewController.h
//  BLEpro
//
//  Created by u on 14-3-6.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoomTextViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *roomText;
@property(strong,nonatomic) NSString *text;
@end
