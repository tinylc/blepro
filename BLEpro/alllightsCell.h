//
//  alllightsCell.h
//  BLEpro
//
//  Created by u on 14-2-28.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "littleXib.h"

@interface alllightsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet littleXib *round;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *room;


-(void)setLabel:(NSString*)kind;
-(void)setState:(int)state;
@end
