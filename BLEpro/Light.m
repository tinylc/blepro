//
//  Light.m
//  BLEpro
//
//  Created by u on 14-6-25.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "Light.h"


@implementation Light

@dynamic lbreathstate;
@dynamic lbright;
@dynamic lcolor;
@dynamic lconfig;
@dynamic lconnect;
@dynamic lgradstate;
@dynamic lID;
@dynamic lname;
@dynamic lnameID;
@dynamic lroom;
@dynamic lrstate;
@dynamic lstate;
@dynamic lwakestate;
@dynamic lwaketime;
@dynamic lrostate;

@end
