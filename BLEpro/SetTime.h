//
//  SetTime.h
//  BLEpro
//
//  Created by u on 14-5-19.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SetTime : NSManagedObject

@property (nonatomic, retain) NSString * tID;
@property (nonatomic, retain) NSString * lID;
@property (nonatomic, retain) NSString * tstate;
@property (nonatomic, retain) NSString * tnum;
@property (nonatomic, retain) NSDate * tdate;

@end
