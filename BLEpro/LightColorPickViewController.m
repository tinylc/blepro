//
//  LightColorPickViewController.m
//  BLEpro
//
//  Created by u on 14-2-23.
//  Copyright (c) 2014年 u. All rights reserved.
//

#import "LightColorPickViewController.h"
#import "UIColor-Expanded.h"
#import "BLEproAppDelegate.h"
@interface LightColorPickViewController (){
    BLEproAppDelegate *blead;
}

@property (strong, nonatomic) IBOutlet UIView *cView;
- (IBAction)saveLightColor:(id)sender;

@end
@implementation LightColorPickViewController

@synthesize delegate;
@synthesize selectedColor;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    blead = [[UIApplication sharedApplication]delegate];
    _lp = [[LightProvider alloc]init];
    _ma=[[ModelAnalyse alloc]init];
    [self.cView setBackgroundColor:[UIColor colorWithRed:(float)48/255 green:(float)56/255 blue:(float)66/255 alpha:1.0]];
    
    KZColorPicker *picker = [[KZColorPicker alloc] initWithFrame:self.view.bounds];
    picker.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.selectedColor = [UIColor colorWithHexString:[_light valueForKey:@"lcolor"]];
    picker.selectedColor =  [UIColor colorWithHexString:[_light valueForKey:@"lcolor"]];
    picker.oldColor =  [UIColor colorWithHexString:[_light valueForKey:@"lcolor"]];
    
	[picker addTarget:self action:@selector(pickerChanged:) forControlEvents:UIControlEventValueChanged];
	[self.cView addSubview:picker];
    
    // Do any additional setup after loading the view.
    //注册刷新回调
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(lrefresh:)
                                                 name:@"lrefresh"
                                               object:nil];
}
-(void)lrefresh:(NSNotification *)notification{
    if(self.view.window!=nil){
        if([[notification object] isEqual:@"powerOff"]||[[notification object] isEqual:@"disconnect"]){
            NSDictionary *theData = [notification userInfo];
            blePeripheral *bp=[theData valueForKey:@"ble"];
            if([[_light valueForKey:@"lnameID"]isEqualToString:bp.uuidString]){
                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"loseConnection",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ok",nil)  otherButtonTitles:nil, nil];
                [alert1 show];
                [alert1 setTag:1];
            }
        }
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==1){
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}
-(BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Lrstatechange"
         object:nil
         userInfo:nil];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) pickerChanged:(KZColorPicker *)cp
{
    self.selectedColor = cp.selectedColor;
	[delegate defaultColorController:self didChangeColor:cp.selectedColor];
    NSLog(@"%@",self.selectedColor.hexStringFromColor);
}

- (CGSize) contentSizeForViewInPopover
{
	return CGSizeMake(320, 416);
}

- (IBAction)saveLightColor:(id)sender {
    [_light setObject:self.selectedColor.hexStringFromColor forKey:@"lcolor"];
    if([[_light valueForKey:@"lstate"]isEqualToString:@"1"]){
        if([[_light valueForKey:@"lgradstate"]isEqualToString:@"0"]){
            [[_lp searchBLEfromLight:_light] InstClickOn:[_light valueForKey:@"lcolor"] WithAlpha:[[_light valueForKey:@"lbright"]intValue]];
        }else{
            [[_lp searchBLEfromLight:_light] GradualClickOn:[_light valueForKey:@"lcolor"] WithAlpha:[[_light valueForKey:@"lbright"]intValue]];
        }
    }
    [_ma changeLight:_light];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
