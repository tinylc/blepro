//
//  blePeripheral.h
//  MonitoringCenter
//
//  Created by David ding on 13-1-10.
//
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <MediaPlayer/MediaPlayer.h>

// 透传模块包含服务
extern NSString *DataServiceUUID;
extern NSString *DeviceServiceUUID;
extern NSString *TimeServiceUUID;


// 指定扫描广播UUID
#define kConnectedServiceUUID                   @"FFB0"



// 消息通知
//==============================================
// 发送消息
#define nPeripheralStateChange                  [[NSNotificationCenter defaultCenter]postNotificationName:@"CBPeripheralStateChange" object:nil userInfo:_bp];

// 接收消息
#define nCBPeripheralStateChange                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CBPeripheralStateChange:) name:@"CBPeripheralStateChange" object:nil];

//==============================================

/****************************************************************************/
/*                      PeripheralDelegateState的类型                        */
/****************************************************************************/
// Peripheral的消息类型
enum {
    blePeripheralDelegateStateDiscoverCharacteristics,
    blePeripheralDelegateStateUpdateCharacteristics,
    blePeripheralDelegateStateUpdate2Characteristics,
    blePeripheralDelegateStateUpdate3Characteristics,
};
typedef NSInteger blePeripheralDelegateState;


@interface blePeripheral : NSObject
//======================================================
// CBPeripheral
@property(strong, nonatomic)    CBPeripheral            *activePeripheral;
//======================================================
// CBService and CBCharacteristic
@property(readonly)             CBService               *DataServiceUUID;
@property(readonly)             CBCharacteristic        *DataCharateristicUUID1;
@property(readonly)             CBCharacteristic        *DataCharateristicUUID2;
@property(readonly)             CBCharacteristic        *DataCharateristicUUID3;
@property(readonly)             CBService               *DeviceServiceUUID;
@property(readonly)             CBCharacteristic        *DeviceCharateristicUUID;
@property(readonly)             CBService               *TimeServiceUUID;
@property(readonly)             CBCharacteristic        *TimeCharateristicUUID1;
@property(readonly)             CBCharacteristic        *TimeCharateristicUUID2;
@property(readonly)             CBCharacteristic        *TimeCharateristicUUID3;
@property(readonly)             CBCharacteristic        *TimeCharateristicUUID4;
@property(readonly)             CBCharacteristic        *TimeCharateristicUUID5;
@property(readonly)             CBCharacteristic        *TimeCharateristicUUID6;
//======================================================
// Property
@property(readonly)             NSUInteger              currentPeripheralState;
@property(readonly)             NSString                *nameString;
@property(readonly)             NSString                *uuidString;
@property(readwrite)            NSString                *staticString;
@property(nonatomic)            NSDictionary     *bp;





//======================================================

// method
-(void)startPeripheral:(CBPeripheral *)peripheral DiscoverServices:(NSArray *)services;
-(void)disconnectPeripheral:(CBPeripheral *)peripheral;
-(void)initPeripheralWithSeviceAndCharacteristic;
-(void)initPropert;
-(void)InstClickOn:(NSString *)color WithAlpha:(int)alpha;
-(void)InstClickOff:(NSString *)color;
-(void)InstClickOff;
-(void)GradualClickOn:(NSString *)color WithAlpha:(int)alpha;
-(void)GradualClickOff:(NSString *)color WithAlpha:(int)alpha;
-(void)breathOpen;
-(void)breathClose;
-(void)wakeUpOpen:(NSDate *)date;
-(void)wakeUpClose;
-(void)sleep;
-(void)writePWMSwitchWithA:(int)a WithB:(int)b WithC:(int)c WithD:(int)d;
-(void)setTime:(NSDate *)date WithNum:(NSString *)num WithGrad:(NSString *)grad WithState:(NSString *)state WithBright:(NSString *)bright;
-(void)writeMethodWithN: (int)n s:(int)s m:(int)m H:(int)H d:(int)d M:(int)M hy:(int)hy ly:(int)ly t:(int)type RGB:(int)rgb Hlength:(int)hlength Llength:(int)llength;
-(void)methodOpenWithTime:(NSMutableArray *)timer WithLight:(NSMutableDictionary *)light;
-(void)methodOpenWithBreath:(NSMutableDictionary *)light;
-(void)openTime:(NSMutableArray *)timer Wake:(NSMutableDictionary *)light;
-(void)readTimeProperties;
@end
