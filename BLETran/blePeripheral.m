//
//  blePeripheral.m
//  MonitoringCenter
//
//  Created by David ding on 13-1-10.
//
//

#import "blePeripheral.h"
#import "BLEproAppDelegate.h"
#import "UIColor-Expanded.h"
#import "DateAnalyse.h"
//================== TransmitMoudel =====================
// Data Service UUID
//控制灯service
NSString *DataServiceUUID                       = @"0xFFB0";
//定时service
NSString *TimeServiceUUID                       = @"0xFE00";
//设备信息service
NSString *DeviceServiceUUID                     = @"0xFF90";

//UUID1  UUID2 颜色 UUID3 时间长度
NSString *DataCharateristicUUID1          = @"0xFFB1";
NSString *DataCharateristicUUID2          = @"0xFFB2";
NSString *DataCharateristicUUID3          = @"0xFFB4";

NSString *DeviceCharateristicUUID         = @"0xFF91";

NSString *TimeCharateristicUUID1          = @"0xFE01";
NSString *TimeCharateristicUUID2          = @"0xFE03";
NSString *TimeCharateristicUUID3          = @"0xFE05";
NSString *TimeCharateristicUUID4          = @"0xFE06";
NSString *TimeCharateristicUUID5          = @"0xFE02";
NSString *TimeCharateristicUUID6          = @"0xFE04";

@implementation blePeripheral{
    NSTimer         *autoSendDataTimer;
    UInt16          testSendCount;
}


#pragma mark -
#pragma mark Init
/******************************************************/
//          类初始化                                   //
/******************************************************/
// 初始化蓝牙
-(id)init{
    self = [super init];
    if (self) {
        [self initPeripheralWithSeviceAndCharacteristic];
        [self initPropert];
    }
    return self;
}
-(void)setActivePeripheral:(CBPeripheral *)AP{
    _activePeripheral = AP;
    NSString *aname = [[NSString alloc]initWithFormat:@"%@",_activePeripheral.name];
    NSLog(@"aname:%@",aname);
    if (![aname isEqualToString:@"(null)"]) {
        _nameString = aname;
    }
    else{
        _nameString = @"Error Name";
    }
    NSString *auuid = [[NSString alloc]initWithFormat:@"%@", _activePeripheral.identifier];
    if (auuid.length >= 36) {
        _uuidString = [auuid substringWithRange:NSMakeRange(auuid.length-36, 36)];
        NSLog(@"uuidString:%@",_uuidString);
    }
}


-(void)initPeripheralWithSeviceAndCharacteristic{
    // CBPeripheral
    [_activePeripheral setDelegate:nil];
    _activePeripheral = nil;
    // CBService and CBCharacteristic
    _DataServiceUUID = nil;
    _DataCharateristicUUID1 = nil;
    _DataCharateristicUUID2 = nil;
}

-(void)initPropert{
    // Property
    _staticString = @"Init";
    //nPeripheralStateChange
}

#pragma mark -
#pragma mark Scanning
/****************************************************************************/
/*						   		Scanning                                    */
/****************************************************************************/
// 按UUID进行扫描
-(void)startPeripheral:(CBPeripheral *)peripheral DiscoverServices:(NSArray *)services{
    if ([peripheral isEqual:_activePeripheral] && [peripheral state]){
        _activePeripheral = peripheral;
        [_activePeripheral setDelegate:(id<CBPeripheralDelegate>)self];
        [_activePeripheral discoverServices:services];
    }
}

-(void)disconnectPeripheral:(CBPeripheral *)peripheral{
    if ([peripheral isEqual:_activePeripheral]){
        // 内存释放
        [self initPeripheralWithSeviceAndCharacteristic];
        [self initPropert];
    }
}

#pragma mark -
#pragma mark CBPeripheral
/****************************************************************************/
/*                              CBPeripheral								*/
/****************************************************************************/
// 扫描服务
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error{
    if (!error)
    {
        if ([peripheral isEqual:_activePeripheral]){
            // 新建服务数组
            NSArray *services = [peripheral services];
            if (!services || ![services count])
            {
                NSLog(@"发现错误的服务 %@\r\n", peripheral.services);
            }
            else
            {
                // 开始扫描服务
                _staticString = @"Discover services";
                //nPeripheralStateChange
                for (CBService *services in peripheral.services)
                {
                    NSLog(@"发现服务UUID: %@\r\n", services.UUID);
                    //================== TransmitMoudel =====================// FFE0
                    if ([[services UUID] isEqual:[CBUUID UUIDWithString:DataServiceUUID]])
                    {
                        // 扫描接收数据服务特征值
                        _DataServiceUUID = services;
                        [peripheral discoverCharacteristics:nil forService:_DataServiceUUID];
                    }
                    if ([[services UUID] isEqual:[CBUUID UUIDWithString:DeviceServiceUUID]])
                    {
                        // 扫描接收数据服务特征值
                        _DeviceServiceUUID = services;
                        [peripheral discoverCharacteristics:nil forService:_DeviceServiceUUID];
                    }
                    if ([[services UUID] isEqual:[CBUUID UUIDWithString:TimeServiceUUID]])
                    {
                        // 扫描接收数据服务特征值
                        _TimeServiceUUID = services;
                        [peripheral discoverCharacteristics:nil forService:_TimeServiceUUID];
                    }
                }
            }
        }
    }
}

// 从服务中扫描特征值
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error{
    if (!error) {
        if ([peripheral isEqual:_activePeripheral]){
            // 开始扫描特征值
            _staticString = @"Discover characteristics";
            
            // 新建特征值数组
            NSArray *characteristics = [service characteristics];
            CBCharacteristic *characteristic;
            //================== TransmitData =====================//
            if ([[service UUID] isEqual:[CBUUID UUIDWithString:DataServiceUUID ]])
            {
                
                for (characteristic in characteristics)
                {
                    [_activePeripheral readValueForCharacteristic:characteristic];
                    if ([[characteristic UUID] isEqual:[CBUUID UUIDWithString:DataCharateristicUUID1]])
                    {
                        _DataCharateristicUUID1 = characteristic;
                        
                        NSLog(@"发现特值1UUID: %@\n", characteristic);
                    }
                    if ([[characteristic UUID] isEqual:[CBUUID UUIDWithString:DataCharateristicUUID2]])
                    {
                        _DataCharateristicUUID2 = characteristic;
                        
                        NSLog(@"发现特值1UUID: %@\n", characteristic);
                    }
                    if ([[characteristic UUID] isEqual:[CBUUID UUIDWithString:DataCharateristicUUID3]])
                    {
                        _DataCharateristicUUID3 = characteristic;
                        
                        NSLog(@"发现特值1UUID: %@\n", characteristic);
                    }
                }
            }
            if ([[service UUID] isEqual:[CBUUID UUIDWithString:DeviceServiceUUID ]])
            {
                for (characteristic in characteristics)
                {
//                     [_activePeripheral readValueForCharacteristic:characteristic];
//                    if ([[characteristic UUID] isEqual:[CBUUID UUIDWithString:DeviceCharateristicUUID]])
//                    {
//                        _DeviceCharateristicUUID = characteristic;
//                        [peripheral setNotifyValue:YES forCharacteristic:characteristic];
//                        NSLog(@"发现特值2UUID: %@\n", characteristic);
//                    }
                }
            }
            if ([[service UUID] isEqual:[CBUUID UUIDWithString:TimeServiceUUID ]])
            {
                for (characteristic in characteristics)
                {
                    [_activePeripheral readValueForCharacteristic:characteristic];
                    if ([[characteristic UUID] isEqual:[CBUUID UUIDWithString:TimeCharateristicUUID1]])
                    {
                        _TimeCharateristicUUID1 = characteristic;
                        [peripheral setNotifyValue:YES forCharacteristic:characteristic];
                        NSLog(@"发现特值3UUID: %@\n", characteristic);
                    }
                    if ([[characteristic UUID] isEqual:[CBUUID UUIDWithString:TimeCharateristicUUID2]])
                    {
                        _TimeCharateristicUUID2 = characteristic;
                        [peripheral setNotifyValue:YES forCharacteristic:characteristic];
                        NSLog(@"发现特值3UUID: %@\n", characteristic);
                    }
                    if ([[characteristic UUID] isEqual:[CBUUID UUIDWithString:TimeCharateristicUUID3]])
                    {
                        _TimeCharateristicUUID3 = characteristic;
                        [peripheral setNotifyValue:YES forCharacteristic:characteristic];
                        NSLog(@"发现特值3UUID: %@\n", characteristic);
                    }
                    if ([[characteristic UUID] isEqual:[CBUUID UUIDWithString:TimeCharateristicUUID4]])
                    {
                        _TimeCharateristicUUID4 = characteristic;
                        [peripheral setNotifyValue:YES forCharacteristic:characteristic];
                        NSLog(@"发现特值3UUID: %@\n", characteristic);
                    }
                    if ([[characteristic UUID] isEqual:[CBUUID UUIDWithString:TimeCharateristicUUID5]])
                    {
                        _TimeCharateristicUUID5 = characteristic;
                        [peripheral setNotifyValue:YES forCharacteristic:characteristic];
                        NSLog(@"发现特值3UUID: %@\n", characteristic);
                    }
                    if ([[characteristic UUID] isEqual:[CBUUID UUIDWithString:TimeCharateristicUUID6]])
                    {
                        _TimeCharateristicUUID6 = characteristic;
                        [peripheral setNotifyValue:YES forCharacteristic:characteristic];
                        NSLog(@"发现特值3UUID: %@\n", characteristic);
                    }
                }
            }
            _bp=[NSDictionary dictionaryWithObject:self forKey:@"bleperipheral"];
            _currentPeripheralState = blePeripheralDelegateStateDiscoverCharacteristics;
            nPeripheralStateChange
            //======================== END =========================
        }
    }
}

// 更新特征值
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    NSLog(@"%@",characteristic);
    if([[ characteristic.UUID UUIDString]isEqualToString: @"FE03"]){
        _currentPeripheralState = blePeripheralDelegateStateUpdateCharacteristics;
        nPeripheralStateChange
    }
    if([[ characteristic.UUID UUIDString]isEqualToString: @"FE05"]){
        _currentPeripheralState = blePeripheralDelegateStateUpdate2Characteristics;
        nPeripheralStateChange
    }
    if([[ characteristic.UUID UUIDString]isEqualToString: @"FFB2"]){
        _currentPeripheralState = blePeripheralDelegateStateUpdate3Characteristics;
        nPeripheralStateChange
    }
}
-(void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    if([error code] == 0){
        
    }else{
        NSLog(@"%@",error);
    }
}
#pragma mark -
#pragma mark read/write/notification
/******************************************************/
//          读写通知等基础函数                           //
/******************************************************/

-(NSMutableDictionary *)colorChange:(NSString *)color{
    UIColor *col = [UIColor colorWithHexString:color];
    NSMutableDictionary *c = [[NSMutableDictionary alloc]init];
    [c setObject:[NSNumber numberWithInt:(int)(col.red*255)] forKey:@"red"];
    [c setObject:[NSNumber numberWithInt:(int)(col.blue*255)] forKey:@"blue"];
    [c setObject:[NSNumber numberWithInt:(int)(col.green*255)] forKey:@"green"];
    return c;
}
-(void)InstClickOn:(NSString *)color WithAlpha:(int)alpha{
    NSMutableDictionary *col;
    col = [self colorChange:color];
    //发送到 0 时间宽度给FFb4 开灯所花时间
    [self writeTimeLengthHigh:0 Low:0];
    //发送到ffb2 调颜色并开灯
    [self writeLightColorWithAlpha:alpha WithRed:[[col valueForKey:@"red"] intValue] WithGreen:[[col valueForKey:@"green"] intValue] WithBlue:[[col valueForKey:@"blue"] intValue]];
    //发送到ffb1 0x02存入当前状态
    //把开灯时的颜色保存到灯的信息中
//    NSData *data4 = [[NSData alloc] initWithBytes:b2 length:sizeof(b2)];
//    [_activePeripheral writeValue:data4 forCharacteristic:_DeviceCharateristicUUID
//                             type:CBCharacteristicWriteWithResponse];
}
-(void)InstClickOff:(NSString *)color{
    NSMutableDictionary *col;
    col = [self colorChange:color];
    //发送到 0 时间宽度给FFb4 关灯所花时间
    [self writeTimeLengthHigh:0 Low:0];
    //发送到ffb1 关灯
    [self writeLightColorWithAlpha:0 WithRed:[[col valueForKey:@"red"] intValue] WithGreen:[[col valueForKey:@"green"] intValue] WithBlue:[[col valueForKey:@"blue"] intValue]];
    //发送到ffb1 0x02存入当前状态
}
-(void)InstClickOff{
    //发送到 0 时间宽度给FFb4 关灯所花时间
    [self writeTimeLengthHigh:0 Low:0];
    //发送到ffb1 关灯
    [self writeLightColorWithAlpha:0 WithRed:0 WithGreen:0 WithBlue:0];
    //发送到ffb1 0x02存入当前状态
}
-(void)GradualClickOn:(NSString *)color WithAlpha:(int)alpha{
    NSMutableDictionary *col;
    col = [self colorChange:color];
    //发送到 10 (0x0064)时间宽度给FFb4 开灯所花时间
    [self writeTimeLengthHigh:0 Low:100];
    //发送到ffb2 调颜色并开灯
    [self writeLightColorWithAlpha:alpha WithRed:[[col valueForKey:@"red"] intValue] WithGreen:[[col valueForKey:@"green"] intValue] WithBlue:[[col valueForKey:@"blue"] intValue]];
    //发送到ffb1 0x02存入当前状态
    //把开灯时的颜色保存到灯的信息中
}
-(void)GradualClickOff:(NSString *)color WithAlpha:(int)alpha{
    //转换颜色
    NSMutableDictionary *col;
    col = [self colorChange:color];
    //发送到 10 (0x0064)时间宽度给FFb4 关灯所花时间
    [self writeTimeLengthHigh:0 Low:100];
    //发送到ffb1 40%亮度
    [self writeLightColorWithAlpha:3 WithRed:(int)([[col valueForKey:@"red"] intValue]*0.1) WithGreen:(int)([[col valueForKey:@"green"] intValue]*0.1) WithBlue:(int)([[col valueForKey:@"blue"] intValue]*0.1)];
    //发送到ffb1 0x02存入当前状态
    //写入当前时间
    [self writeNowDate];
    //获取10秒后的时间
    NSDate *future = [NSDate dateWithTimeIntervalSinceNow:10];
    //写入事件 10秒后亮度突变为0% FE03
    [self writeMethodWithNumber:0 WithDate:future WithType:4 WithRGB:0 WithHlength:0 WithLlength:0];
//    //4路pwm打开事件03 && 打开4路的定时功能
//    [self writePWMSwitchWithA:0x01 WithB:0 WithC:0 WithD:0];
}
//读取数据
-(void)readTimeProperties{
    int j=8;
    for(int i=0;i<7;i++){
        [self writeTimePoint:j];
        [_activePeripheral readValueForCharacteristic:_TimeCharateristicUUID2];
        if(i==2){
            j=j+6;
        }else{
            j++;
        }
    }
}
-(void)wakeUpOpen:(NSDate *)date{
    //写入当前时间
    [self writeNowDate];
    //写入事件 定时0秒调整为10% FE03
    DateAnalyse *da = [[DateAnalyse alloc]init];
    [self writeMethodWithN:8 s:0 m:[da getmmfromdate:date] H:[da getHHfromdate:date] d:255 M:255 hy:255 ly:255 t:4 RGB:1 Hlength:0 Llength:0];
    //写入事件 定时30秒内调整为70%
    [self writeMethodWithN:9 s:0 m:[da getmmfromdate:date] H:[da getHHfromdate:date] d:255 M:255 hy:255 ly:255 t:5 RGB:100 Hlength:1 Llength:44];
    //写入事件 定时90秒后调整为100%
    NSDate *future =[NSDate dateWithTimeInterval:90 sinceDate:date];
    [self writeMethodWithN:10 s:30 m:[da getmmfromdate:future] H:[da getHHfromdate:future] d:255 M:255 hy:255 ly:255 t:5 RGB:255 Hlength:1 Llength:44];
//    //4路pwm打开事件03 && 打开4路的定时功能
//    [self writePWMSwitchWithA:0 WithB:255 WithC:0 WithD:0];
}
-(void)wakeUpClose{
    //4路pwm打开事件03 && 打开4路的定时功能
    [self writePWMSwitchWithA:0 WithB:0 WithC:0 WithD:0];
}
-(void)breathOpen{
    //写入当前时间
    [self writeNowDate];
    //读取当前时间的秒数和30秒之后的秒数  //[da getssfromdate:now]; //[da getssfromdate:future];
    DateAnalyse *da = [[DateAnalyse alloc]init];
    for(int i=0;i<12;i++){
        NSDate *future = [NSDate dateWithTimeIntervalSinceNow:5*i];
        //调色 40~100%
        //写入事件 每分钟的第几秒开始5秒后亮度变为100 FE03
        [self writeMethodWithN:i s:[da getssfromdate:future] m:255 H:255 d:255 M:255 hy:255 ly:255 t:5 RGB:255 Hlength:0 Llength:50];
        future = [NSDate dateWithTimeIntervalSinceNow:5*i+5];
        //调色 100%~40
        //写入事件 每分钟的第几秒开始5秒后亮度变为40% FE03
        [self writeMethodWithN:i+1 s:[da getssfromdate:future] m:255 H:255 d:255 M:255 hy:255 ly:255 t:5 RGB:0 Hlength:0 Llength:50];
        i++;
    }

}
-(void)breathClose{
     //4路pwm打开事件03 && 打开4路的定时功能
    for(int i=0;i<24;i++){
        [self writeMethodWithN:i s:0 m:0 H:0 d:0 M:0 hy:0 ly:0 t:0 RGB:0 Hlength:0 Llength:0];
    }
}
-(void)setTime:(NSDate *)date WithNum:(NSString *)num WithGrad:(NSString *)grad WithState:(NSString *)state WithBright:(NSString *)bright{
    int method=[num intValue]+16;
    int s =[grad intValue]+4;
    int t = 0;
    if(s==5){
        t=100;
    }
    int b=0;
    if([state intValue]==1){
        b=[bright intValue];
    }
    
    //写入当前时间
    [self writeNowDate];
    //写入事件 定时事件号 分钟 小时 渐变 状态 亮度 FE03
    DateAnalyse *da = [[DateAnalyse alloc]init];
    NSLog(@"%@ %@ %@ %@ %d ",num,grad,state,bright,[da getmmfromdate:date]);
    if([state intValue] !=2){
        [self writeMethodWithN:method s:0 m:[da getmmfromdate:date] H:[da getHHfromdate:date] d:255 M:255 hy:255 ly:255 t:s RGB:b Hlength:0 Llength:t];
    }else{
        [self writeMethodWithN:method s:0 m:0 H:0 d:0 M:0 hy:0 ly:0 t:0 RGB:0 Hlength:0 Llength:0];
    }
    //    //4路pwm打开事件03 && 打开4路的定时功能
    //    [self writePWMSwitchWithA:0 WithB:255 WithC:0 WithD:0];
}
-(void)sleep{
    //写入时间长度
    [self writeTimeLengthHigh:0 Low:0];
    //写入当前颜色值
    [self writeLightColorWithAlpha:255 WithRed:255 WithGreen:255 WithBlue:255];
    //写入当前时间
    [self writeNowDate];
    //调色 100%~40%~100%~40%~100%~40%~100%~40%
    for(int i=0;i<7;i++){
        //获取i分钟后的时间
        NSDate *future = [NSDate dateWithTimeIntervalSinceNow:60*i];
        //写入事件 i分钟后亮度突变为40%或者100% FE03
        if(i%2==0){
            [self writeMethodWithNumber:(24+i) WithDate:future WithType:5 WithRGB:5 WithHlength:2 WithLlength:88];
        }else if(i%2==1){
            [self writeMethodWithNumber:(24+i) WithDate:future WithType:5 WithRGB:255 WithHlength:2 WithLlength:88];
        }
    }
    //调色 40%~0
    //获取 7分钟后的时间
    NSDate *future = [NSDate dateWithTimeIntervalSinceNow:60*7];
    //写入事件 7分钟后亮度突变为0 FE03
    [self writeMethodWithNumber:31 WithDate:future WithType:5 WithRGB:0 WithHlength:2 WithLlength:88];
//    //4路pwm打开事件03 && 打开4路的定时功能
//    [self writePWMSwitchWithA:0 WithB:0 WithC:0 WithD:255];
}
//写入当前时间
-(void)writeNowDate{
    NSDate *now = [NSDate date];
    NSLog(@"%@",now);
    DateAnalyse *dateAnalyse = [[DateAnalyse alloc]init];
    Byte b9[] = {[dateAnalyse getssfromdate:now],[dateAnalyse getmmfromdate:now],[dateAnalyse getHHfromdate:now],[dateAnalyse getddfromdate:now],[dateAnalyse getMMfromdate:now],[dateAnalyse getyyfromdate:now],(int)[dateAnalyse getyyfromdate:now]/256};
    NSData *data9 = [[NSData alloc] initWithBytes:b9 length:sizeof(b9)];
    [_activePeripheral writeValue:data9 forCharacteristic:_TimeCharateristicUUID1
                             type:CBCharacteristicWriteWithResponse];
}
//写入读取某事件的指针
-(void)writeTimePoint:(int)num{
    Byte b1[]={num};
    NSData *data1 = [[NSData alloc] initWithBytes:b1 length:sizeof(b1)];
    [_activePeripheral writeValue:data1 forCharacteristic:_TimeCharateristicUUID5
                             type:CBCharacteristicWriteWithResponse];
}
//写入读取某端口的指针{
-(void)writeTimePoint2{
    Byte b1[]={8};
    NSData *data1 = [[NSData alloc] initWithBytes:b1 length:sizeof(b1)];
    [_activePeripheral writeValue:data1 forCharacteristic:_TimeCharateristicUUID6
                             type:CBCharacteristicWriteWithResponse];
}
//写入开关灯时间长度
-(void)writeTimeLengthHigh:(int)hLength Low:(int)lLength{
    Byte b1[]={hLength,lLength};
    NSData *data1 = [[NSData alloc] initWithBytes:b1 length:sizeof(b1)];
    [_activePeripheral writeValue:data1 forCharacteristic:_DataCharateristicUUID3
                             type:CBCharacteristicWriteWithResponse];
}
//写入开关灯颜色
-(void)writeLightColorWithAlpha:(int)alpha WithRed:(int)red WithGreen:(int)green WithBlue:(int)blue {
    Byte b2[] = { alpha,red,green,blue};
    NSData *data2 = [[NSData alloc] initWithBytes:b2 length:sizeof(b2)];
    [_activePeripheral writeValue:data2 forCharacteristic:_DataCharateristicUUID2
                             type:CBCharacteristicWriteWithResponse];
}
//写入事件1 (参数 事件号、触发日期、触发类型、触发rgb百分比、灯变化时间长度高低位)
-(void)writeMethodWithNumber: (int)num WithDate:(NSDate *)date WithType:(int)type WithRGB:(int)rgb WithHlength:(int)hlength WithLlength:(int)llength {
    DateAnalyse *dateAnalyse = [[DateAnalyse alloc]init];
    Byte b3[] = {num,[dateAnalyse getssfromdate:date],[dateAnalyse getmmfromdate:date],[dateAnalyse getHHfromdate:date],[dateAnalyse getddfromdate:date],[dateAnalyse getMMfromdate:date],[dateAnalyse getyyfromdate:date],(int)[dateAnalyse getyyfromdate:date]/256,type,rgb,llength,hlength};
    NSData *data3 = [[NSData alloc] initWithBytes:b3 length:sizeof(b3)];
    [_activePeripheral writeValue:data3 forCharacteristic:_TimeCharateristicUUID2
                             type:CBCharacteristicWriteWithResponse];
}
//写入事件2 (参数 事件号、秒、分、时、日、月、年、触发类型、触发rgb百分比、灯变化时间长度高低位) (主要针对日期部分为ff时，意思为每小时或者每分钟、每天必然发生的事情)
-(void)writeMethodWithN: (int)n s:(int)s m:(int)m H:(int)H d:(int)d M:(int)M hy:(int)hy ly:(int)ly t:(int)type RGB:(int)rgb Hlength:(int)hlength Llength:(int)llength {
    Byte b3[] = {n,s,m,H,d,M,hy,ly,type,rgb,llength,hlength};
    NSData *data3 = [[NSData alloc] initWithBytes:b3 length:sizeof(b3)];
    [_activePeripheral writeValue:data3 forCharacteristic:_TimeCharateristicUUID2
                             type:CBCharacteristicWriteWithResponse];
}
//打开某个事件的4路开关
-(void)writePWMSwitchWithA:(int)a WithB:(int)b WithC:(int)c WithD:(int)d{
    //4路pwm打开事件03
    Byte b4[] = {0x06,a,b,c,d};
    NSData *data4 = [[NSData alloc] initWithBytes:b4 length:sizeof(b4)];
    [_activePeripheral writeValue:data4 forCharacteristic:_TimeCharateristicUUID3
                             type:CBCharacteristicWriteWithResponse];
    Byte b5[] = {0x07,a,b,c,d};
    NSData *data5 = [[NSData alloc] initWithBytes:b5 length:sizeof(b5)];
    [_activePeripheral writeValue:data5 forCharacteristic:_TimeCharateristicUUID3
                             type:CBCharacteristicWriteWithResponse];
    Byte b6[] = {0x08,a,b,c,d};
    NSData *data6 = [[NSData alloc] initWithBytes:b6 length:sizeof(b6)];
    [_activePeripheral writeValue:data6 forCharacteristic:_TimeCharateristicUUID3
                             type:CBCharacteristicWriteWithResponse];
    Byte b7[] = {0x09,a,b,c,d};
    NSData *data7 = [[NSData alloc] initWithBytes:b7 length:sizeof(b7)];
    [_activePeripheral writeValue:data7 forCharacteristic:_TimeCharateristicUUID3
                             type:CBCharacteristicWriteWithResponse];
    //打开4路的定时功能
    Byte b8[] = {0x81,0x07};
    NSData *data8 = [[NSData alloc] initWithBytes:b8 length:sizeof(b8)];
    [_activePeripheral writeValue:data8 forCharacteristic:_TimeCharateristicUUID4
                             type:CBCharacteristicWriteWithResponse];
}
//事件打开管理
-(void)methodOpenWithTime:(NSMutableArray *)timer WithLight:(NSMutableDictionary *)light{
    [self writePWMSwitchWithA:255 WithB:[self judgemethodOpen:@"lwakestate" WithLight:light] WithC:[self judgemethodOpenWithTimer:timer]  WithD:255];
}
-(void)methodOpenWithBreath:(NSMutableDictionary *)light{
    [self writePWMSwitchWithA:255 WithB:255 WithC:255 WithD:255];
}
-(int)judgemethodOpen:(NSString *)n WithLight:(NSMutableDictionary *)light{
    if([[light valueForKey:n]isEqualToString:@"0"]){
        return 0;
    }
    return 255;
}
-(int)judgemethodOpenWithTimer:(NSMutableArray *)timer{
    int k=0;
    int j=1;
    for(int i=0;i<timer.count;i++){
        if(![[timer[i] valueForKey:@"lstate"]isEqualToString:@"2"]){
            k=j+k;
            j=j*2;
        }
    }
    return k;
}
-(void)openTime:(NSMutableArray *)timer Wake:(NSMutableDictionary *)light{
    for(int i=0;i<timer.count;i++){
        if([[[timer objectAtIndex:i]valueForKey:@"tstate"]isEqualToString:@"2"]){
            [self writeMethodWithN:16+i s:0 m:0 H:0 d:0 M:0 hy:0 ly:0 t:0 RGB:0 Hlength:0 Llength:0];
        }else{
            [self setTime:[[timer objectAtIndex:i ] valueForKey:@"tdate"] WithNum:[[timer objectAtIndex:i ] valueForKey:@"tnum"] WithGrad:[light valueForKey:@"lgradstate"] WithState:[[timer objectAtIndex:i ] valueForKey:@"tstate"] WithBright:[light valueForKey:@"lbright"]];
        }
    }
    [self wakeUpOpen:[light valueForKey:@"lwaketime"]];
}
#pragma mark -
#pragma mark Set property
/******************************************************/
//              BLE属性操作函数                          //
/******************************************************/





@end
